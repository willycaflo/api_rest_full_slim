<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../src/config/db.php';

$app = new \Slim\App;

$app->get('/hola/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hola, $name !!");

    return $response;
});

$app->get('/prueba/{uno}/{dos}', function (Request $request, Response $response) {
    $uno = $request->getAttribute('uno');
    $dos = $request->getAttribute('dos');
    /*$response->getBody()->write("$uno y  $dos");
    return $response;*/
    echo $uno ." y ".$dos;
});

//Crear las rutas para el cliente
require "../src/rutas/clientes.php";

$app->run();
