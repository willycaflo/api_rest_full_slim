<?php
/**
 *
 */
class db {
  private $host = 'localhost';
  private $usuario = 'root';
  private $password = 'mysql';
  private $base = 'api_slim';

  public function conectar(){
    $conexion_mysql = "mysql:host=$this->host;dbname=$this->base";
    $conexionBD = new PDO($conexion_mysql,$this->usuario,$this->password);
    $conexionBD->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    //Forza las codificacion de caracteres UTF-8
    $conexionBD->exec("SET NAMES utf8");
    return $conexionBD;
  }
}
?>
