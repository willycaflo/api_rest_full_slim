<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

//Obtener todos los clientes
$app->get('/api/clientes', function(Request $request,Response $response){
    $consulta = 'SELECT * FROM clientes';
    try {
      //Instanciacion de las base de datos
      $db = new db();

      $db = $db->conectar();
      $ejecutar = $db->query($consulta);
      $clientes = $ejecutar->fetchAll(PDO::FETCH_OBJ);
      $db = null;

      //Mostrar en JSON
      echo json_encode($clientes);

    } catch (PDOException $e) {
      echo '{"error":{"text":'.$e->getMessage().'}}';
    }
});

//Obtener un cliente por su id
$app->get('/api/clientes/{id}', function(Request $request,Response $response){
    $id = $request->getAttribute('id');
    $consulta = 'SELECT * FROM clientes WHERE id_cliente = '.$id;
    try {
      //Instanciacion de las base de datos
      $db = new db();

      $db = $db->conectar();
      $ejecutar = $db->query($consulta);
      $cliente = $ejecutar->fetchAll(PDO::FETCH_OBJ);
      $db = null;

      //Mostrar en JSON un solo cliente
      echo json_encode($cliente);

    } catch (PDOException $e) {
      echo '{"error":{"text":'.$e->getMessage().'}}';
    }
});

//Agregar un cliente
$app->post('/api/clientes/agregar', function(Request $request,Response $response){
    $nombre = $request->getParam('nombre');
    $apellidos = $request->getParam('apellidos');
    $telefono = $request->getParam('telefono');
    $email = $request->getParam('email');
    $direccion = $request->getParam('direccion');
    $ciudad = $request->getParam('ciudad');
    $deaprtamento = $request->getParam('deaprtamento');
    //print_r($nombre);
    $consulta = "INSERT INTO clientes(nombre,apellidos,telefono,email,direccion,ciudad,deaprtamento) VALUES
    (:nombre, :apellidos, :telefono, :email, :direccion, :ciudad, :deaprtamento)";
    try {
      //Instanciacion de las base de datos
      $db = new db();
      $db = $db->conectar();
      $stmt = $db->prepare($consulta);
      $stmt->bindParam(':nombre',$nombre,PDO::PARAM_STR);
      $stmt->bindParam(':apellidos',$apellidos,PDO::PARAM_STR);
      $stmt->bindParam(':telefono',$telefono,PDO::PARAM_STR);
      $stmt->bindParam(':email',$email,PDO::PARAM_STR);
      $stmt->bindParam(':direccion',$direccion,PDO::PARAM_STR);
      $stmt->bindParam(':ciudad',$ciudad,PDO::PARAM_STR);
      $stmt->bindParam(':deaprtamento',$deaprtamento,PDO::PARAM_STR);
      $stmt->execute();
      echo '{"notice":{"text": "Cliente agregado"}}';

    } catch (PDOException $e) {
      echo '{"error":{"text":'.$e->getMessage().'}}';
    }
});

//Actualizar los datos de un cliente por id
$app->put('/api/clientes/actualizar/{id}', function(Request $request,Response $response){
    $id = $request->getAttribute('id');

    $nombre = $request->getParam('nombre');
    $apellidos = $request->getParam('apellidos');
    $telefono = $request->getParam('telefono');
    $email = $request->getParam('email');
    $direccion = $request->getParam('direccion');
    $ciudad = $request->getParam('ciudad');
    $deaprtamento = $request->getParam('deaprtamento');
    //print_r($nombre);
    $consulta = "UPDATE clientes
                SET
                  nombre = :nombre,
                  apellidos = :apellidos,
                  telefono = :telefono,
                  email = :email,
                  direccion = :direccion,
                  ciudad = :ciudad,
                  deaprtamento = :deaprtamento
                WHERE id_cliente = $id";
    try {
      //Instanciacion de las base de datos
      $db = new db();
      $db = $db->conectar();
      $stmt = $db->prepare($consulta);
      $stmt->bindParam(':nombre',$nombre,PDO::PARAM_STR);
      $stmt->bindParam(':apellidos',$apellidos,PDO::PARAM_STR);
      $stmt->bindParam(':telefono',$telefono,PDO::PARAM_STR);
      $stmt->bindParam(':email',$email,PDO::PARAM_STR);
      $stmt->bindParam(':direccion',$direccion,PDO::PARAM_STR);
      $stmt->bindParam(':ciudad',$ciudad,PDO::PARAM_STR);
      $stmt->bindParam(':deaprtamento',$deaprtamento,PDO::PARAM_STR);
      $stmt->execute();
      echo '{"notice":{"text": "Cliente actualizado"}}';

    } catch (PDOException $e) {
      echo '{"error":{"text":'.$e->getMessage().'}}';
    }
});

//Eliminar a un cliente por su id
$app->delete('/api/clientes/borrar/{id}', function(Request $request,Response $response){
    $id = $request->getAttribute('id');
    $consulta = 'DELETE FROM clientes WHERE id_cliente = '.$id;
    try {
      //Instanciacion de las base de datos
      $db = new db();

      $db = $db->conectar();
      $stmt = $db->prepare($consulta);
      $stmt->execute();
      $db = null;

      //Mostrar mensaje
      echo '{"notice":{"text": "Cliente borrado"}}';

    } catch (PDOException $e) {
      echo '{"error":{"text":'.$e->getMessage().'}}';
    }
});

?>
