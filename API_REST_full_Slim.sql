CREATE DATABASE api_slim;
USE api_slim;

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `ciudad` varchar(45) DEFAULT NULL,
  `deaprtamento` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `clientes` VALUES (1,'Jose','Montoya','33588942','josemon@gmail.com','Calle 23# 78','Pereira','Risaralda'),
(2,'Martha','Cardenas','3569874','matha_car@hotmail.com','Carrera 7 # 89','Cali','Valle');
